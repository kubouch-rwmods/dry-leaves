# Changelog

## [unreleased]
none

## [b18.1.0] - 2017-11-18
* Official beta 18 (0.18.1722) release (no changes from previous version)

## [a18.0.1] - 2017-11-13
* First release: Alpha 18 (0.8.1719, unstable)
* Contains full Jecrell's tobacco mod.
* Smokeleaf and tobacco leaves need to be dried before rolling them.
* Requires Universal Fermenter
